FROM 100.125.5.235:20202/caoyongsheng/as-docker-go:1.11.2-1.8.5

COPY ./godi /home
COPY ./conf /home/conf
RUN chmod +x /home/godi

CMD ["/home/godi"]